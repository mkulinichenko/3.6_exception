package ua.pp.voronin.serhii.tommy;

public class ExceptionMaker {

    public static void main(String[] args) {
        new ExceptionMaker().isThereTwo(getData());
    }

    public boolean isThereTwo(Object[] objectsArray) throws SaferException {
        try {
            for (Object someObject : objectsArray) {
                if (someObject.equals(2)) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            throw new SaferException("NullPointerException occurred", e);
        }
    }
    public static Object[] getData() {
        return new Object[] { new Object(), null, "test data", 2 };
    }
}
