package ua.pp.voronin.serhii.tommy;

// Змінити клас так, щоб його можна було використовувати у якості винятка
public class SaferException extends RuntimeException{
    public SaferException() {
        super();
    }

    public SaferException(String message) {
        super(message);
    }

    public SaferException(String message, Throwable cause) {
        super(message, cause);
    }

    public SaferException(Throwable cause) {
        super(cause);
    }
}
